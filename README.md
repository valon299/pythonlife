# pythonlife

### An AquaLife port to python

In AquaLife several aquariums are simulated with the help of a distributed system. Fish can be created with a mouse click and have the possibility to swim from one aquarium to another.

PythonLife now ports AquaLife from Java to Python and replaces fish with snakes. PyGame is used for visualization.