import pygame, random

speed = 3
bg = [234, 215, 129]

pygame.init()
screen = pygame.display.set_mode((600, 350))
pygame.display.set_caption("PythonLife")
clock = pygame.time.Clock()
mister_slither = pygame.image.load("slither_scaled.png")
screen.fill(bg)
slithers = []
they_see_me_slithin = []
	
while True:
	screen.fill(bg)
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			exit()
		if event.type == pygame.MOUSEBUTTONUP:
			pos = pygame.mouse.get_pos()
			slithers.append((pos[0]-50, pos[1]-35, random.randint(0, 1)))

	for s in slithers:
		if s[2] == 0:
			screen.blit(mister_slither, (s[0], s[1]))
			they_see_me_slithin.append((s[0]+speed, s[1], 0))
		else:
			they_see_me_slithin.append((s[0]-speed, s[1], 1))
			screen.blit(
				pygame.transform.flip(mister_slither, True, False),
				(s[0], s[1]))
	slithers = they_see_me_slithin	
	they_see_me_slithin = []
	for s in slithers:
		if (s[0] > 700) or (s[0] < -100):
			slithers.remove(s)
	clock.tick(60)
	pygame.display.update()